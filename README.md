Note: This is just a practice chart and it is NOT MAINTAINED!

Based on Matomo docker 4.4.1
https://github.com/matomo-org/docker

and the guide at

https://docs.bitnami.com/tutorials/create-your-first-helm-chart/


# MVP helm chart for Matomo

```
matomo-chart-mvp
|-- Chart.yaml
|-- templates
|   |-- NOTES.txt
|   |-- _helpers.tpl
|   |-- deployment.yaml
|   |-- ingress.yaml
|   `-- service.yaml
`-- values.yaml
```
